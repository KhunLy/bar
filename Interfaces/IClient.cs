﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_InterfaceBis.Interfaces
{
    interface IClient
    {
        /// <summary>
        /// Le montant du protefeuille ne peut pas etre neg 
        /// </summary>
        double PorteFeuille { get; set; }
        string Nom { get; set; }
        List<IBiere> Bieres { get; }

        /// <summary>
        /// Permet d'ajouter une bière dans la liste de bières
        /// </summary>
        /// <param name="b"></param>
        void Ajouter(string nom, double prix);

        /// <summary>
        /// Vérifie que le montant total soit inf au portefeuille
        /// Déduit le montant du portefeuille
        /// Vide la liste des bières à payer
        /// </summary>
        void Payer();

        /// <summary>
        /// Retourne la somme totale des bières de la liste
        /// </summary>
        /// <returns></returns>
        double GetMontant();
    }
}
