﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoOO_InterfaceBis.Interfaces
{
    interface IBiere
    {
        string Nom { get; set; }
        /// <summary>
        /// > 0
        /// </summary>
        double PourcentageAlcool { get; set; }
        /// <summary>
        /// > 0
        /// </summary>
        double Prix { get; set; }
    }
}
